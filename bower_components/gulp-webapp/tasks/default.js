/*
 * default.js
 *
 * Gulp default Task.
 *
 */

var gulp = require('gulp');

/**
 * Task: default
 *
 * Run serve task.
 */
gulp.task('default', ['serve']);