app = angular.module('starter.controllers', ['checklist-model', 'ionic-toast']);
app.controller('TabCtrl', function($scope, $localstorage) {});
app.controller('ReviewCtrl', function($scope, $ionicModal, $localstorage, Items, Controls, Reviews) {
    var yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
    $scope.today = new Date();
    $scope.items = Items;
    $scope.controls = Controls;
    $scope.myControls = $localstorage.getControls();
    try {
        $scope.myControls = $localstorage.getControls();
    } catch(err) {
        console.log("error:" + err + " " + JSON.stringify(err));
    }
    $scope.reviews = Reviews.getList();
    $scope.review = {
        control: null,
        reviewDate: null,
        part: null,
        issue: false,
        remarks: null,
        state: 0
    };
    $scope.enviar = function() {
        alert("Thanks ");
    };
    $scope.datepickerObject = {
        titleLabel: 'Fecha de Control', //Optional
        closeLabel: 'Cerrar', //Optional
        setLabel: 'Ok', //Optional
        setButtonType: 'button-balanced', //Optional
        closeButtonType: 'button-assertive', //Optional
        inputDate: yesterday, //Optional
        mondayFirst: true, //Optional
        templateType: 'popup', //Optional
        //   showTodayButton: false, //Optional
        modalHeaderColor: 'bar-positive', //Optional
        modalFooterColor: 'bar-positive', //Optional
        from: new Date(2012, 8, 2), //Optional
        to: new Date(2018, 8, 25), //Optional
        callback: function(val) { //Mandatory
            $scope.review.reviewDate = val;
        },
        dateFormat: 'dd-MM-yyyy', //Optional
        closeOnSelect: false, //Optional
    };
    /*Agregar */
    $ionicModal.fromTemplateUrl('templates/add-review.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.addReview = modal;
    });
    $scope.openAddReview = function() {
        $scope.addReview.show();
    };
    $scope.closeAddReview = function() {
        $scope.addReview.hide();
    };
    $scope.pushReview = function() {
        $scope.today = new Date();
        $scope.reviews.$add({
            "user": "prueba",
            "control": $scope.review.control,
            "reviewDate": $scope.review.reviewDate.getTime(),
            "part": $scope.review.part,
            "issue": $scope.review.issue,
            "remarks": $scope.review.remarks,
            "state": 0,
            "timestamp": Firebase.ServerValue.TIMESTAMP,
            "priority": 0 - $scope.today.valueOf()
        }).then(function() {
            $scope.review = {
                control: null,
                reviewDate: null,
                part: null,
                issue: false,
                remarks: null
            };
            $scope.closeAddReview();
        });
    };
});
app.controller('IssuesCtrl', function($scope, Issues, ionicToast, $localstorage) {
    $scope.issues = Issues.getList();
    $scope.fix = function(issueId) {
        if($localstorage.isAdmin()) {
            angular.forEach($scope.issues, function(value, key) {
                if(value.$id === issueId) {
                    value.issueStatus = 2;
                    Issues.fix(value.$id);
                }
            });
        } else {
            ionicToast.show('No tiene permisos para realizar esta operación', 'top', false, 3000);
        }
    };
});
app.controller('FixesCtrl', function($scope, Fixes) {
    $scope.fixes = Fixes.getList();
    $scope.unfix = function(issueId) {
        angular.forEach($scope.fixes, function(value, key) {
            if(value.$id === issueId) {
                value.issueStatus = 1;
                Fixes.unfix(value.$id);
            }
        });
    };
});
app.controller('PendingCtrl', function($scope, Pending, ionicToast, $localstorage) {
    $scope.pendings = Pending.getList();
    $scope.check = function(issueId) {
        if($localstorage.isAdmin()) {
        angular.forEach($scope.pendings, function(value, key) {
            //console.log("issueId " + issueId + " ValueId: " + value.$id);
            if(value.$id === issueId) {
                value.state = 1;
                //  console.log("Value: " + JSON.stringify(value));
                Pending.doCheck(value.$id);
            }
        });
            } else {
            ionicToast.show('No tiene permisos para realizar esta operación', 'top', false, 3000);
        }
    };
});
app.controller('CheckedCtrl', function($scope, Checked) {
    $scope.checkedList = Checked.getList();
    $scope.uncheck = function(itemId) {
        //    console.log("Clicked ");
        angular.forEach($scope.checkedList, function(value, key) {
            //   console.log("itemId "+itemId+" ValueId: "+ value.$id);
            if(value.$id === itemId) {
                value.state = 0;
                //           console.log("Value: "+ JSON.stringify(value));
                Checked.unCheck(value.$id);
            }
        });
    };
});
app.controller('IssuesDetailCtrl', function($scope, $stateParams, Chats) {
    $scope.chat = Chats.get($stateParams.chatId);
});
app.controller('AccountCtrl', function($scope, $rootScope, $window, Controls, $localstorage) {
    if($localstorage.getObject('settings')) {
        $scope.settings = $localstorage.getObject('settings');
    } else {
        $scope.settings = {
            admin: false,
            user: "",
            pass: "",
            controls: []
        };
    }
    $scope.errors = [];
    $scope.controls = Controls;
    $scope.clear = function() {
        $localstorage.removeAll();
        $rootScope.logged = false;
        $scope.settings = {
            admin: false,
            user: "",
            pass: "",
            controls: []
        };
    };
    $scope.start = function() {
        if($scope.save()) {
            $window.location.href = '#/tab/review';
        }
    };
    $scope.save = function() {
        $scope.errors = [];
        if($scope.settings.user == null || $scope.settings.user.length < 5) {
            $scope.errors.push("Su usuario no es válido");
        }
        if($scope.settings.admin && $scope.settings.pass != "hola76!") {
            $scope.errors.push("Su usuario no es administrador");
        }
        if(!$scope.settings.admin && $scope.settings.controls.length < 1) {
            $scope.errors.push("No ha seleccionado ningún control");
        }
        if($scope.settings.admin && $scope.settings.controls.length >= 1) {
            $scope.errors.push("No puede tener controles asignados");
        }
        if($scope.errors.length == 0) {
            console.log("todo bien, guardar");
            $rootScope.logged = true;
            $localstorage.setObject('settings', $scope.settings);
            return true;
        }
    };
});