var app = angular.module('starter.services', []);
app.config(function($httpProvider) {
    $httpProvider.interceptors.push(function($rootScope) {
        return {
            request: function(config) {
                $rootScope.$broadcast('loading:show')
                return config
            },
            response: function(response) {
                $rootScope.$broadcast('loading:hide')
                return response
            }
        }
    })
});
app.factory("$localstorage", ['$window',
    function($window) {
        var settings = {};
        return {
            setObject: function(key, value) {
                settings = value;
                $window.localStorage[key] = JSON.stringify(value);
            },
            getControls: function() {
                return settings.controls;
            },
            getUser: function() {
                return settings.user;
            },
            isAdmin: function() {
                return settings.admin;
            },
            getObject: function(key) {
                if($window.localStorage[key]) {
                    return JSON.parse($window.localStorage[key] || '{}');
                } else {
                    return false;
                }
            },
            removeAll: function() {
                //$window.localStorage = [];
                $window.localStorage['settings'] = [];
                settings = {};
            }
        };
    }
]);
app.factory("Items", function($firebaseArray) {
    var itemsRef = new Firebase("https://controls-ba.firebaseio.com/items");
    return $firebaseArray(itemsRef);
});
app.factory("Auth", function($firebaseAuth) {
    var usersRef = new Firebase("https//controls-ba.firebaseio.com/users");
    return $firebaseAuth(usersRef);
});
app.factory("Controls", function($firebaseArray) {
    var itemsRef = new Firebase("https://controls-ba.firebaseio.com/controls");
    return $firebaseArray(itemsRef);
});
app.factory("Reviews", function($firebaseArray) {
    var itemsRef = new Firebase("https://controls-ba.firebaseio.com/reviews").orderByChild("priority").limitToFirst(300);
    return {
        getList: function() {
            return $firebaseArray(itemsRef)
        },
        getId: function(id) {
            var revRef = new Firebase("https://controls-ba.firebaseio.com/reviews").orderByChild("$id").equalTo(id);
            return $firebaseArray(revRef);
        }
    };
    // return $firebaseArray(itemsRef);
});
app.factory("Issues", function($firebaseArray) {
    var itemsRef = new Firebase("https://controls-ba.firebaseio.com/reviews").orderByChild("issueStatus").equalTo(1);
     return {
        getList: function() {
            return $firebaseArray(itemsRef);
        },
        fix: function(id) {
            var revRef = new Firebase("https://controls-ba.firebaseio.com/reviews").child(id);
            revRef.update({
                "issueStatus": 2,
                "fixDate": Firebase.ServerValue.TIMESTAMP
            });
        }
    };
});
app.factory("Fixes", function($firebaseArray) {
    var itemsRef = new Firebase("https://controls-ba.firebaseio.com/reviews").orderByChild("issueStatus").equalTo(2);
    return {
        getList: function() {
            return $firebaseArray(itemsRef);
        },
        unfix: function(id) {
            var revRef = new Firebase("https://controls-ba.firebaseio.com/reviews").child(id);
            revRef.update({
                "issueStatus": 1,
                "fixDate": null
            });
        }
    };
});
app.factory("Pending", function($firebaseArray) {
    var itemsRef = new Firebase("https://controls-ba.firebaseio.com/reviews").orderByChild("state").equalTo(0);
    return {
        getList: function() {
            return $firebaseArray(itemsRef);
        },
        doCheck: function(id) {
            var revRef = new Firebase("https://controls-ba.firebaseio.com/reviews").child(id);
            revRef.update({
                "state": 1,
                "checkDate": Firebase.ServerValue.TIMESTAMP
            });
        }
    };
});
app.factory("Checked", function($firebaseArray) {
    var itemsRef = new Firebase("https://controls-ba.firebaseio.com/reviews").orderByChild("state").equalTo(1);
    return {
        getList: function() {
            return $firebaseArray(itemsRef)
        },
        unCheck: function(id) {
            var revRef = new Firebase("https://controls-ba.firebaseio.com/reviews").child(id);
            revRef.update({
                "state": 0,
                "checkDate": null
            });
        }
    };
});