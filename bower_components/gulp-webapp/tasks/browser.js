/*
 * browser.js
 * ===========
 * Open default browser.
 *
 */

var gulp        = require('gulp'),
    openBrowser = require('gulp-open'),
    config      = require('../config');

/**
 * Task: browser
 *
 * Open default browser and load index.html page.
 */
gulp.task('browser', ['http'], function() {
    gulp.src(".tmp/index.html")
        .pipe(openBrowser("", config.browser));
});