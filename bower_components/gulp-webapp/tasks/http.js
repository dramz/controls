/*
 *http.js
 *
 * Run http mini server.
 *
 */

var gulp        = require('gulp'),
    connect     = require('gulp-connect'),
    config      = require('../utils/configuration')();

gulp.task('http', ['inject'], function() {
    connect.server(config.http);
});