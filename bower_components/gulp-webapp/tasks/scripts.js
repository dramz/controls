/*
 * javascript.js
 *
 * Javascript Task for inject all into index.html file.
 * require('jshint-stylish')
 */
var gulp                   = require('gulp'),
    del                    = require('del'),
    gutil                  = require('gulp-util'),
    filter                 = require('gulp-filter'),
    gulpif                 = require('gulp-if'),
    jshint                 = require('gulp-jshint'),
    uglify                 = require('gulp-uglify'),
    htmlmin                = require('gulp-htmlmin'),
    revison                = require('gulp-rev'),
    concat                 = require('gulp-concat'),
    rename                 = require("gulp-rename"),
    angularFilesort        = require('gulp-angular-filesort'),
    angularTemplateCache   = require('gulp-angular-templatecache'),
    vulcanize              = require('gulp-vulcanize'),
    bowerFiles             = require('main-bower-files'),
    config                 = require('../utils/configuration')();


/**
 * Helper: _cleanScripts
 *
 * Clean .tmp/scripts folder
 */
gulp.task('_cleanScripts', function() {
    return del.sync(['.tmp/**/*.js']);
});



/**
 * Helper: cleanCSS
 *
 * Clean .tmp/styles/css folder
 */
gulp.task('_jshint', ['_cleanScripts'], function () {
    if (config.jshint.enabled) {
        gutil.log(gutil.colors.green('JS Hint active.'));
    } else {
        gutil.log(gutil.colors.red('JS Hint deactivated.'));
    }

    return gulp.src('./source/**/*.js')
        .pipe( gulpif(config.jshint.enabled, jshint(), gutil.noop()) )
        .pipe( gulpif(config.jshint.enabled, jshint.reporter('jshint-stylish'), gutil.noop()) )
        .pipe( gulpif(config.jshint.enabled, jshint.reporter('fail'), gutil.noop()) );
});



/**
 * Helper: _angularTemplateCache
 *
 * Copy .html templates to .tmp and keep file structure, minify content.
 */
gulp.task('_angularTemplateCache', ['_cleanScripts', '_jshint'], function() {
    if (config.state == 'build') {
        return gulp.src('./source/**/*.html')
            .pipe(htmlmin(config.htmlmin))
            .pipe(angularTemplateCache({standalone: true, module: config.templatesModule, root: './'}))
            .pipe(rename({prefix: "_"}))
            .pipe(gulp.dest('./.tmp'));
    }

    return true;
});



/**
 * Helper: _concatScripts
 *
 * Concat all js scripts and minify them on build and add revision number to file name.
 */
gulp.task('_concatScripts', ['_cleanScripts', '_jshint', '_angularTemplateCache'], function() {
    if ((config.state == 'build') && (config.angular.templateCache)) {
        return gulp.src(['./source/**/*.js','./.tmp/**/*.js'])
            .pipe( gulpif(config.angular.fileSort, angularFilesort()) )
            .pipe(concat('app.min.js'))
            .pipe(uglify(config.uglify))
            .pipe(revison())
            .pipe(gulp.dest('./.tmp'));
    }

    return true;
});


/**
 * Helper: _copyScriptTemplates
 *
 * Copy .html templates to .tmp and keep file structure, minify content.
 */
gulp.task('_copyScriptTemplates', ['_cleanScripts', '_jshint'], function() {
    if (config.state == 'build') {
        return gulp.src('./source/**/*.html')
            .pipe(htmlmin(config.htmlmin))
            .pipe(gulp.dest('./.tmp'));
    }

    return true;
});




/* Gulp Tasks *********************************************************************************************************/

/**
 * Task: JavaScript
 * Run javascript helper tasks for handle all different javascript tasks.
 * Concat the result and inject into index.html file on build.
 *
 * Possible helpers:
 *   _jshint                Handle bower components css files.
 *
 */

gulp.task('scripts', ['_cleanScripts', '_jshint', '_concatScripts', '_copyScriptTemplates'], function() {
    //
});