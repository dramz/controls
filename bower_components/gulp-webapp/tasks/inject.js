/*
 * inject.js
 *
 * Inject Task for inject all into index.html file.
 *
 */

var gulp                   = require('gulp'),
    inject                 = require('gulp-inject'),
    del                    = require('del'),
    gulpif                 = require('gulp-if'),
    htmlmin                = require('gulp-htmlmin'),
    bowerFiles             = require('main-bower-files'),
    angularFilesort        = require('gulp-angular-filesort'),
    config                 = require('../utils/configuration')();

/**
 * Task: inject
 *
 * Inject CSS/JS into a copy of index.html file. Located on .tmp folder.
 */
gulp.task('inject', ['css','scripts'], function() {
    // cleanup previous created _CSS files inside temp folder.
    del.sync(['.tmp/**/_*.*', '.source/**/_*.*']);

    // inject
    return gulp.src('app/index.html')
        // add all source css and js files
        .pipe(inject( gulp.src(['.tmp/**/*.{css,js}'], {read: false}), {ignorePath: ['.tmp'], addRootSlash: false}) )
        // on serve add all js files from origin folder
        .pipe( gulpif((config.state == 'serve'),
            inject( gulp.src(['source/**/*.js'], {read: false})
                    .pipe(gulpif(config.angular.fileSort ,angularFilesort())),
                {ignorePath: ['source'], addRootSlash: false})) )
        // inject all bower.json files if enabled
        .pipe( gulpif(config.inject.bower, inject( gulp.src(bowerFiles(), {read: false}), { ignorePath: ['app'], addRootSlash: false, name: 'bower'})) )
        // on build minimize index.html
        .pipe( gulpif((config.state == 'build'), htmlmin(config.htmlmin)) )
        .pipe(gulp.dest('.tmp'));
});
