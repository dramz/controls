module.exports = function(params) {
    var glob     = require("glob"),
        fs       = require("fs"),
        ncp      = require('ncp').ncp,
        exec     = require('child_process').exec,
        files    = [],
        packages = [],
        path     = __dirname;

    // init params
    if (typeof params === 'undefined') {
        params = [];
    }


// js files
    files = files.concat(glob.sync(path + "/tasks/**/*.js"));
    files = files.concat(glob.sync(path + "/utils/**/*.js"));
    files.push(path + '/template/gulpfile.js');

// invoke js files and find required packages
    files.forEach(function(file) {
        var data = fs.readFileSync(file, "utf8");

        data.replace(/require\(['"]([^./][\s\S]+?)['"]\)/ig, function () {
            //arguments[0] is the entire match

            if (packages.indexOf(arguments[1]) == -1) {
                packages.push(arguments[1]);
            }
        });
    });

// install packages
    if (params.indexOf('--no-package-install') === -1) {
        console.log('Installing package: ' + packages.join(' '));
        exec('npm install ' + packages.join(' ') + ' --save-dev', function (error, stdout, stderr) {
            // output is in stdout
            console.log(stdout);
        });
    }

// copy template files
    if (params.indexOf('--no-template-files') === -1) {
        console.log('Copy template files.');
        ncp(path + '/template/', './', function (err) {
            if (err) {
                return console.error(err);
            }
            console.log('done!');
        });
    }
};