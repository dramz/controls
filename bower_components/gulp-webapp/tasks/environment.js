/*
 * environment.js
 *
 * Set environment variable for BUILD or SERVE tasks. Default is BUILD.
 *
 */

var gulp        = require('gulp'),
    config      = require('../utils/configuration')();

/**
 * Helper: state-serve
 *
 * Set the state to "serve"
 */
gulp.task('_state-serve', function() {
    return config.state = "serve";
});

/**
 * Helper: state-build
 *
 * Set the state to "build"
 */
gulp.task('_state-build', function() {
    return config.state = "build";
});