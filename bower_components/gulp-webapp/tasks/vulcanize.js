/*
 * javascript.js
 *
 * Javascript Task for inject all into index.html file.
 * require('jshint-stylish')
 */
var gulp                   = require('gulp'),
    del                    = require('del'),
    lodash                 = require('lodash'),
    vulcanize              = require('gulp-vulcanize'),
    config                 = require('../utils/configuration')();

var print = require('gulp-print');

/**
 * Task: vulcanizePolymer
 *
 *
 */
gulp.task('vulcanizePolymer', ['_preBuild'], function() {
    var DEST_DIR   = './dist',
        opt = { dest: DEST_DIR };

    if (config.state == 'build') {
        return gulp.src('./dist/index.html')
            .pipe(vulcanize(lodash.merge(opt, config.vulcanize)))
            .pipe(gulp.dest(DEST_DIR));
    }

    return true;
});