/*
 * css.js
 *
 * Build SASS files, autoprefix all CSS files, concate all CSS files.
 *
 */

var gulp            = require('gulp'),
    del             = require('del'),
    gutil           = require('gulp-util'),
    filter          = require('gulp-filter'),
    gulpif          = require('gulp-if'),
    concat          = require('gulp-concat'),
    inject          = require('gulp-inject'),
    revison         = require('gulp-rev'),
    autoprefixer    = require('gulp-autoprefixer'),
    minifyCSS       = require('gulp-minify-css'),
    sass            = require('gulp-sass'),
    bowerFiles      = require('main-bower-files'),
    config          = require('../utils/configuration')(),
    handleErrors    = require('../utils/error-handle');


/**
 * Helper: cleanCSS
 *
 * Clean .tmp/styles/css folder
 */
gulp.task('_cleanCSS', function() {
    return del.sync('.tmp/styles/css');
});


/**
 * Helper: CSS pure
 *
 * Use css files inside style folder.
 * Autoprefix and minify not minified files and merge them all.
 */
gulp.task('_css-pure', ['_cleanCSS'], function() {
    // filter for css not minified files
    var cssMinFilter = filter(['**/*.css', '!**/*.min.css']),
        cssFilter = filter(['**/*.css']);

    return gulp.src('styles/css/**/*.css')
        .pipe(cssMinFilter)
        .pipe( autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }) )
        .pipe( gulpif((config.state == 'build'), minifyCSS()) )
        .pipe(cssMinFilter.restore())
        .pipe(cssFilter)
        .pipe(concat( gulpif((config.state == 'build'), '_css.min.css', '_css.css') ))
        .pipe(gulp.dest('.tmp/styles/css'));
});

/**
 * Helper: CSS SASS
 *
 * Use sass files inside style folder using compass.
 * Autoprefix and minify not minified files and merge them all.
 */
gulp.task('_css-sass', ['_cleanCSS'], function() {
    return gulp.src('styles/sass/**/*.scss')
        .pipe(sass())
        .on('error', handleErrors)
        .pipe( autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }) )
        .pipe( gulpif((config.state == 'build'), minifyCSS()) )
        .pipe(concat( gulpif((config.state == 'build'), '_sass.min.css', '_sass.css') ))
        .pipe(gulp.dest('.tmp/styles/css'));
});







/* Gulp Tasks *********************************************************************************************************/

/**
 * Task: CSS
 * Run css helper tasks for handle all different css tasks.
 * Concat the result and inject into index.html file.
 *
 * Possible helpers:
 *   _css-pure              Handle clean css files.
 *   _css-sass              Handle SASS (scss) and css files.
 *
 */
gulp.task('css', ['_css-sass', '_css-pure'], function() {
    return gulp.src('.tmp/styles/css/_*.css')
        .pipe( concat( gulpif((config.state == 'build'), 'main.min.css', 'main.css') ))
        .pipe( gulpif((config.state == 'build'), revison()) )
        .pipe( gulp.dest('.tmp/styles/css'))
});

