/*
 * serve.js
 *
 * Serve application, not minified and open in browser.
 *
 */

var gulp                   = require('gulp'),
    gutil                  = require('gulp-util'),
    connect                = require('gulp-connect'),
    config                 = require('../utils/configuration')();

/**
 * Task: Serve
 *
 * watch for changes on source folder and file types css,scss,js
 */
gulp.task('serve', ['_state-serve', 'http','inject','browser' ], function() {
    var watchPaths = ['source/**/*.{' + config.watch.sourceFileTypes.join(',') + '}',
        'styles/**/*.{' + config.watch.sourceFileTypes.join(',') + '}',
        'app/index.html',
        'bower.json'];

    // extend watch path if defined in config file
    if (Object.prototype.toString.call(config.watch.paths) == '[object Array]') {
        watchPaths = watchPaths.concat(config.watch.paths);
    }

    var CSSWatcher = gulp.watch(watchPaths, ['serve-refresh']);

    gulp.task('serve-refresh', ['inject'], function() {
        gulp.src('.tmp/*.html').pipe(connect.reload());
    });

    CSSWatcher.on('change', function (event) {
        gutil.log(gutil.colors.green('File ' + event.path + ' was ' + event.type + ', running tasks...'));
    });
});