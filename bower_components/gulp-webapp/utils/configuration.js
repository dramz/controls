var config = require('../config'),
    lodash = require('lodash');

module.exports = function() {
   if (typeof configuration !== 'undefined') {
       return lodash.merge(config, configuration);
   } else {
       return config;
   }
};