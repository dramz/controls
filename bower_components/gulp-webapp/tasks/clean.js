/*
 * clean.js
 *
 * Remove all previous files created by the tasks.
 *
 */

var gulp        = require('gulp'),
    del         = require('del');

/**
 * Task: clean
 *
 * Clean .tmp and dist folder
 */
gulp.task('clean', function() {
    return del.sync(['.tmp', 'dist']);
});