/*
 * config.js
 *
 * gulp-webapp default config settings.
 *
 */

module.exports = {
    http: {
        port: 8000,
        root: ['.tmp','source','app'],
        livereload: true
    },
    browser: {
        url: "http://localhost:8000"
        //app: "firefox"
    },
    jshint: {
        enabled: true
    },
    inject: {
        bower: true
    },
    uglify: {
        mangle: false
    },
    htmlmin: {
        collapseWhitespace: true,
        removeComments:     true
    },
    angular: {
        fileSort: false,
        templateCache: true
    },
    watch: {
        sourceFileTypes: ['css','scss','js'],
        paths: []
    },
    vulcanize: {
        strip: true
    },
    state: 'build'
};