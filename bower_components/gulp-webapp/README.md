Gulp-WebApp
===========

Gulp-WebApp help creating web application using [Gulp](http://gulpjs.com/) build system.

Gulp-WebApp can be adapted to individual requirements. These are not changed during updates.
Predefined tasks can be easily adjusted via a configuration to your own needs.

## Installation
You can install Gulp-WebApp in two ways. Manually for existing projects or you use [WebApp Starter Package](https://github.com/nahody/webapp) if you are creating a new Application.

#### WebApp Starter Package
Please refer the the [WebApp Starter Package](https://github.com/nahody/webapp) manual.

#### Manual
In case you want to add it to your existing project, follow these steps.
You need [bower](http://bower.io/) and [node](http://nodejs.org/) manifest files. If they not exists already, create them now.
For bower use `bower init`, for Node use `npm init`.
Gulp-WebApp expect that all your static files exists in a subfolder called `app`. Also the `bower_components` folder has to resist here.
Here for create following `.bowerrc` file:
```json
{
  "directory": "./app/bower_components"
}
```

When both manifest and .bowerrc files exist you can start installing required packages.
```shell
bower install gulp-webapp --save-dev
npm install ncp glob --save-dev
```

## Update
To update Gulp-WebApp simply use `bower update`. Sometimes it may be necessary that additional node dependent packages to be installed.
Here for run `node bower_components/gulp-webapp/update` inside your project. This will add all dependent packages to your
node manifest file and install them.

## File Structure
```
Project
├── .bowerrc
├── bower.json
├── package.json
├── app
|   ├── bower_components
|   ├── styles
|   ├── scripts
|   ├── manifest.webapp
|   ├── manifest.json
|   ├── chromeapp.js
|   ├── browserconfig.xml
|   ├── crossdomain.xml
|   ├── humans.txt
|   ├── favicon.ico
|   └── index.html
├── source
|   ├── footer.html
|   └── header.html
├── styles
|   ├── css
|   ├── scss
|   └── images
└── gulpfile.js
```

## Configuration Options
** Default Settings **
```javascript
{
    http: {
        port: 8000,
        root: ['.tmp','source','app'],
        livereload: true
    },
    browser: {
        url: "http://localhost:8000"
        //app: "firefox"
    },
    jshint: {
        enabled: true
    },
    inject: {
        bower: true
    },
    uglify: {
        mangle: false
    },
    htmlmin: {
        collapseWhitespace: true,
        removeComments:     true
    },
    angular: {
        fileSort: false,
        templateCache: true
    },
    watch: {
        sourceFileTypes: ['css','scss','js'],
        paths: []
    },
    vulcanize: {
        strip: true
    }
}
```

#### http - HTTP Server and Livereload
[Gulp-Connect](https://github.com/avevlad/gulp-connect) parameters. For detailed description and API refer to the project homepage.

| Predefined Option              | Description     | Default |
|--------------------------------|-----------------|---------|
|`port` |The connect webserver port.| 8000 |
|`root` |The root path.| ['.tmp','source','app'] |
|`livereload` |Enable/Disable live reload.| true |

#### browser - Open Browser
[Gulp-Open](https://github.com/stevelacy/gulp-open) parameters. For detailed description and API refer to the project homepage.

| Predefined Option              | Description     | Default |
|--------------------------------|-----------------|---------|
|`url` | URLs may not have a default application. If the task is running without opening in a browser try setting the options.app. | http://localhost:8000 |

#### polymer - Polymer Vulcanize
[Gulp-Vulcanize](https://github.com/sindresorhus/gulp-vulcanize) parameters. For detailed description and API refer to the [origin project homepage](https://github.com/Polymer/vulcanize).

| Predefined Option              | Description     | Default |
|--------------------------------|-----------------|---------|
|`strip` | Remove comments and empty text nodes.| true |

#### htmlmin - Minimize and Uglify HTML Documents
[Gulp-htmlmin](https://github.com/jonschlinkert/gulp-htmlmin) parameters. For detailed description and API refer to the project homepage.

| Predefined Option              | Description     | Default |
|--------------------------------|-----------------|---------|
|`collapseWhitespace` | Collapse whitespaces. | true |
|`removeComments` | Remove comments. | true |

#### jshint
[gulp-jshint](gulp-jshint) no direct parameters are possible.

| Option                         | Description     | Default |
|--------------------------------|-----------------|---------|
|`enabled` | Enable or disable JavaScript Hint.| true |

#### Inject

| Option                         | Description     | Default |
|--------------------------------|-----------------|---------|
|`bower` | Inject bower files into index.html file.| true |

#### Angular JS
[gulp-angular-filesort]() no direct parameters are possible.    
[gulp-angular-templatecache]() no direct parameters are possible.

| Option                         | Description     | Default |
|--------------------------------|-----------------|---------|
|`fileSort` | Use gulp-angular-filesort.| true |
|`templateCache` | Use gulp-angular-templatecache.| true |

#### Uglify Javascript
[gulp-uglify](https://github.com/terinjokes/gulp-uglify) parameters. For detailed description and API refer to the project homepage.

| Predefined Option              | Description     | Default |
|--------------------------------|-----------------|---------|
|`mangle` |Pass false to skip mangling names.| false |

#### Watch
Watch defines the file extensions to look for on default path. Additional path with file extension/s can be defined separately.

| Option                         | Description     | Default |
|--------------------------------|-----------------|---------|
|`sourceFileTypes` | File extensions to look for. | ['css','scss','js'] |
|`paths` | Additional path with file extensions. | [] |


## Predefined Tasks
| Task                         | Description     |
|--------------------------------|-----------------|
|browser||
|build||
|clean||
|css||
|environment||
|http||
|inject||
|scripts||
|serve||
|vulcanize||