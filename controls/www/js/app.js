// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'firebase', 'ionic-datepicker'])

.run(function($ionicPlatform, $rootScope, $ionicModal, $localstorage) {
    $rootScope.getItemHeight = function(item) {
        if(item.issue) {
            return 115;
        } else {
            return 95;
        }
    };
    if($localstorage.getUser()){
        $rootScope.logged = true;
    }else{
        $rootScope.logged = false;
    };
    /*Ver y borrar */
    $ionicModal.fromTemplateUrl('templates/review-detail.html', {
        scope: $rootScope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $rootScope.reviewDetail = modal;
    });
    $rootScope.openReviewDetail = function(item) {
       // angular.forEach($scope.reviews, function(value, key) {
       //     console.log("itemId " + itemId + " ValueId: " + value.$id);
       //     if(value.$id === itemId) {
                $rootScope.activeItem = item;
                $rootScope.reviewDetail.show();
       //     }
      //  });
    };
    $rootScope.closeReviewDetail = function() {
        $rootScope.reviewDetail.hide();
    };

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'TabCtrl'
  })



  .state('tab.review', {
    url: '/review',
    views: {
      'tab-review': {
        templateUrl: 'templates/tab-review.html',
        controller: 'ReviewCtrl'
      }
    }
  })

  .state('tab.issues', {
      url: '/issues',
      views: {
        'tab-issues': {
          templateUrl: 'templates/tab-issues.html',
          controller: 'IssuesCtrl'
        }
      }
  })
  .state('tab.fixes', {
      url: '/fixes',
      views: {
        'tab-fixes': {
          templateUrl: 'templates/tab-fixes.html',
          controller: 'FixesCtrl'
        }
      }
  })
  .state('tab.pending', {
      url: '/pending',
      views: {
        'tab-pending': {
          templateUrl: 'templates/tab-pending.html',
          controller: 'PendingCtrl'
        }
      }
    })
  .state('tab.checked', {
      url: '/checked',
      views: {
        'tab-checked': {
          templateUrl: 'templates/tab-checked.html',
          controller: 'CheckedCtrl'
        }
      }
    })


  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/account');

});
