/**
 * build.js
 *
 * Build application in minified version.
 *
 */

var gulp            = require('gulp'),
    del             = require('del');

/**
 * Helper: _preBuild
 *
 */
gulp.task('_preBuild', ['_state-build', 'inject'], function() {
    // clear dist and .tmp folder
    del.sync(['./dist']);

    // copy files into dist folder
    return gulp.src(['./.tmp/**/*', './app/**/*', '!./app/index.html'])
        .pipe(gulp.dest('./dist'));
});

/**
 * Task: Build
 *
 */
gulp.task('build', ['_preBuild', 'vulcanizePolymer'], function() {
    // clear .tmp folder so it has clean state for the next process
    del.sync(['./.tmp']);
});