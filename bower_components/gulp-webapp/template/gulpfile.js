/*
 gulpfile.js
 ===========
 Rather than manage one giant configuration file responsible for creating multiple tasks,
 each task has been broken out into its own file in gulp-webapp/tasks.
 Any files in that directory get automatically required below.

 gulp/tasks/default.js specifies the default set of tasks to run when you run `gulp`.

 To overwrite default setting, extend `global.configuration = {}` object. Possible setting
 can we viewed on default `gulp-webapp/config.js` file.
 */

var requireDir = require('require-dir');

// Overwrite default configuration
global.configuration = {};

// Require all tasks in gulp/tasks, including subfolders
requireDir('./app/bower_components/gulp-webapp/tasks', { recurse: true });
